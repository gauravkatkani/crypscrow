<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//frontend
Route::get('/','FrontendController@index' );
Route::get('terms-and-policy','FrontendController@terms' );
Route::get('contactus','FrontendController@contactus' );
Route::get('registers','FrontendController@register');
Route::get('logins','FrontendController@login');
Route::post('logins','FrontendController@postLogin')->name('logins');
Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['auth']], function () {
	Route::resource('profile','ProfileController');
});
Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor');
Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor');
Route::get('/2fa/validate', 'Auth\LoginController@getValidateToken');
Route::post('/2fa/validate', ['middleware' => 'throttle:5', 'uses' => 'Auth\LoginController@postValidateToken']);

Route::group(['middleware' => ['web']], function () {
   
    Route::get('admin/login', 'Admin\Auth\RegisterController@getAdminLogin');

    Route::post('admin/login', ['as'=>'admin.auth','uses'=>'Admin\Auth\RegisterController@adminAuth']);

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('userverify/{magiclink}', 'Auth\LoginController@verifyUser')->name('userverify');

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {


    	Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

		Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');
	    
	    Route::group(['middleware' => ['admin']], function () {

	    	Route::get('dashboard', ['as'=>'admin.dashboard','uses'=>'HomeController@dashboard']);

	    });
	});
    Route::group(['prefix' => 'crypscrow-admin', 'namespace' => 'SuperAdmin'], function () {

	    Route::get('/', 'Auth\LoginController@showLoginForm')->name('super-admin');

	    Route::post('login', 'Auth\LoginController@login')->name('super-admin.login');

	    Route::get('logout', 'Auth\LoginController@logout')->name('super-admin.logout');

	    Route::post('logout', 'Auth\LoginController@logout')->name('super-admin.logout');

	    Route::group(['middleware' => 'super-admin.auth'], function() {

	    	Route::get('/home', 'HomeController@index')->name('super-admin.home');
	    	
	    	Route::get('/user', 'UserControlller@index')->name('super-admin.user.index');

	    	Route::get('/user/ajax', 'UserControlller@ajaxLoad')->name('super-admin.user.ajax');

	    });

});
    
});