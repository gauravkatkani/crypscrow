<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class RegisterController extends Controller
{
	use AuthenticatesUsers;
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
    */
    
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    public function getAdminLogin()
    {
        
        if (auth()->guard('admin')->user()) 
    	{
    		return redirect()->route('admin.dashboard');
    	}
        return view('layouts.adminlogin');
    }
    public function adminAuth(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')]))
        {
            return redirect()->route('admin.dashboard');
        }else{
            dd('your username and password are wrong.');
        }
    }
}
