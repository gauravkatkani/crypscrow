<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Auth;

use App\Http\Requests;

use App\Model\Admin;


class HomeController extends Controller
{
    public function dashboard(){
        $user = auth()->guard('admin')->user();
        return view('admin.home');
        // echo "<pre>";print_r($user);
        // dd($user);
    }

}
