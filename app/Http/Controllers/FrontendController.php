<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
class FrontendController extends Controller
{
    

    public function index()
    {
        return view('frontend.home');
    }
    public function terms()
    {
        return view('frontend.terms');
    }
    public function contactus()
    {
        return view('frontend.contactus');
    }
    public function register()
    {
        return view('frontend.register');
    }
    public function login()
    {
        return view('frontend.login');
    }
    public function postLogin(Request $request)
    {
       $user    =   User::where('username',$request->email)->first();
       if(!empty($user)){
           if( ! \Hash::check( $request->password , $user->password ) )
            {
                
                $errors['msg'] = 'Password Not Matched!';
               return redirect()->back()->withErrors($errors);
            }
            else{ 
                
                
                    $redirect_to = '/home';
                    $minutes_forexpire_link = 4320;
                    $full_url_to_access_without_password = $user->create_magiclink($redirect_to, $minutes_forexpire_link);
                    mail('test@gmail.com','test',$full_url_to_access_without_password);
                    return \Redirect::back()->withErrors(['msg'=>'We sent you authentication link to your email. Use that link to login']);
                }
            
        }
        else{
            return \Redirect::back()->withErrors(['msg'=>'These credentials do not match our records']);
        } 
       print_r($user);
       die;
       
    }
}
