<?php

namespace App\Http\Controllers\SuperAdmin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserControlller extends Controller
{
    //
    public function index(){
    	return view('super-admin.users.index');
    }
    public function ajaxLoad(){
    	 $records     =   array();

        // Fetch all languages from database
        $users  =   User::all();

        // Intialize varible
        $i          =   0;

        foreach ($users as $user) {
        	// Language title
            $records[$i]['username']    =      $user->username;
            

            // Number of patients under in language
            $records[$i]['email']      =      $user->email;

            $records[$i]['action']      =      'coming soon';
            $records[$i]['privilege']      =      'coming soon';

            $i++;
        }
        $columns 	=	array(array('data'=>'username'),array('data'=>'email'),array('data'=>'action'),array('data'=>'privilege'));
    	return \Response::json(compact('records','columns'));
    	
    }
}
