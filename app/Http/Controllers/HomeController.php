<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $google2fa = new Google2FA();
    
        $secretKey = $google2fa->generateSecretKey();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl(
        'YourCompany',
        Auth::user()->email,
        $secretKey
    );
        return view('home',compact('secretKey','google2fa_url'));
    }
}
