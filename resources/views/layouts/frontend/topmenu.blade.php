<div class="mainmenu-area stricky">
		    <div class="container">
		    	<div class="row">
		    		<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="main-logo">
							<!-- <a href="index-2.html"><img src="images/logo/logo.png" alt=""></a> -->
							<a href="index-2.html"><img src="{!! asset('frontend/images/footer/logo1.png')!!}" alt=""></a>
						</div>
					</div>
					<div class="">
	                    
	                    <aside class="col-md-6 col-sm-6 ">
	                        <ul class="crypto-user-section">
	                             @guest
		                            <li> <a href="{{url('logins')}}" ><i class="fa fa-user"></i> Login</a></li>
		                            <li> <a href="{{url('registers')}}" ><i class="fa fa-sign-in"></i> Sign Up</a></li>
		                        @else
		                        	<li> 
		                        		<a href="javascript:void(0);" >
		                        			<i class="fa fa-user"></i>  
		                        			{{ Auth::user()->username }}
		                        		</a>
		                        	</li>
		                        	<li> <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form></li>
		                        @endguest
	                        </ul>
	                    </aside>
	                </div>
				</div>
		        
		    </div>
		</div>