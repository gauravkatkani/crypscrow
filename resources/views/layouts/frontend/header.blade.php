<!DOCTYPE html>
<html lang="zxx">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="UTF-8">
	<title>CrypScrow</title> 
	  <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="{!! asset('frontend/css/style.css')!!}">
	<!-- <link rel="stylesheet" href="css/responsive.css"> -->
	<!-- <link rel="stylesheet" href="fonts/flaticon.css" /> -->
	<!--favicon-->
	<link rel="apple-touch-icon" sizes="180x180" href="{!! asset('frontend/images/favicon/apple-touch-icon.png')!!}">
	<link rel="icon" type="image/png" href="{!! asset('frontend/images/favicon/favicon-32x32.png')!!}" sizes="32x32">
	<link rel="icon" type="image/png" href="{!! asset('frontend/images/favicon/favicon-16x16.png')!!}" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="{!! asset('frontend/css/particle.css')!!}">
	
</head>

<body>
	<div class="boxed_wrapper">