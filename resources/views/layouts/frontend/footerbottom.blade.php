

		<!-- Scroll Top Button -->
		<button class="scroll-top tran3s color2_bg">
			<span class="fa fa-angle-up"></span>
		</button>
		<!-- pre loader  -->
		<div class="preloader"></div>

	</div>

	<!-- jQuery js -->
	<script src="{!! asset('frontend/js/jquery.js')!!}"></script>
	<!-- bootstrap js -->
	<script src="{!! asset('frontend/js/bootstrap.min.js')!!}"></script>
	<!-- jQuery ui js -->
	<script src="{!! asset('frontend/js/jquery-ui.js')!!}"></script>
	
	<script src="{!! asset('frontend/js/custom.js')!!}"></script>
	 
	<!-- particles.js lib - https://github.com/VincentGarreau/particles.js --> 
	<script src="{!! asset('frontend/js/particles.min.js')!!}"></script>
	<script src="{!! asset('frontend/js/particle.js')!!}"></script>
	 {!! NoCaptcha::renderJs() !!}
	  @stack('scripts')
</body>

</html>