<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layouts.backend.admin.header')
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        @include('layouts.backend.admin.sidebar')
        @include('layouts.backend.admin.topmenu')
        <!-- page content -->
        <div class="right_col" role="main">
            <!-- page content -->
            @yield('content')
        <!-- /page content -->
        </div>
        <!-- /page content -->
        @include('layouts.backend.admin.footer')
        
      </div>
    </div>

    <!-- jQuery -->
    <script src="{{url('backend/vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{url('backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{url('backend/vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{url('backend/vendors/nprogress/nprogress.js')}}"></script>
    <!-- Chart.js -->
    <script src="{{url('backend/vendors/Chart.js/dist/Chart.min.js')}}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{url('backend/vendors/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
    <!-- Flot -->
    <script src="{{url('backend/vendors/Flot/jquery.flot.js')}}"></script>
    <script src="{{url('backend/vendors/Flot/jquery.flot.pie.js')}}"></script>
    <script src="{{url('backend/vendors/Flot/jquery.flot.time.js')}}"></script>
    <script src="{{url('backend/vendors/Flot/jquery.flot.stack.js')}}"></script>
    <script src="{{url('backend/vendors/Flot/jquery.flot.resize.js')}}"></script>
    <!-- Flot plugins -->
    <script src="{{url('backend/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
    <script src="{{url('backend/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
    <script src="{{url('backend/vendors/flot.curvedlines/curvedLines.js')}}"></script>
    <!-- DateJS -->
    <script src="{{url('backend/vendors/DateJS/build/date.js')}}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{url('backend/vendors/moment/min/moment.min.js')}}"></script>
    <script src="{{url('backend/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="{{url('backend/build/js/custom.min.js')}}"></script>
  </body>
</html>