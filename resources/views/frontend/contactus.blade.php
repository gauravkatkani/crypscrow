@extends('layouts.frontend.masterone')
@section('content')
<section class="about">
			<div class="container">
				<div class="item-list">
					<div class="row">
						
						<div class="col-md-7 col-sm-12 col-xs-12">
			                <div class="sec-title">
								<h3 class="left">Help and support</h3><br/>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit perferendis aliquam, ducimus explicabo voluptas odit ad amet ratione delectus laborum aperiam veniam, optio facilis sapiente maiores deleniti porro, inventore doloremque Lorem ipsum dolor sit amet, consectetur adipisicing.</p><br/>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit perferendis aliquam, ducimus explicabo voluptas odit ad amet ratione delectus laborum aperiam veniam, optio facilis sapiente maiores deleniti porro, inventore doloremque Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
							</div>
			                
			                <ul class="s-list list-unstyled mb-20">
			                  <li><span class="fa fa-check"></span>Donec facilisis velit eu est phasellus consequat quis nostrud</li>
			                  <li><span class="fa fa-check"></span>Aenean vitae quam. Vivamus et nunc nunc conseq</li>
			                  <li><span class="fa fa-check"></span>Sem vel metus imperdiet lacinia enea sapiente maior</li>
			                  <li><span class="fa fa-check"></span>Dapibus aliquam augue fusce eleifend quisque tels</li>
			                  <li><span class="fa fa-check"></span>Dapibus aliquam augue fusce tels optio facilis sapiente maiores</li>
			                </ul>
			                <ul class="about-links text-left">
								<li><a href="https://localethereum.zendesk.com/hc/en-us" class="thm-btn style-two">OPEN A SUPPORT TICKET</a></li>
								
							</ul>

			              </div>
						<div class="col-md-5 col-sm-10 col-xs-12">
							<!-- <div class="item">
								<figure class="image-box">
									<img src="images/about/1.png" alt="" />
								</figure>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection		