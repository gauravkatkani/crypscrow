@extends('layouts.frontend.masterone')
@section('content')
<section class="about">
			<div class="container">
				<div class="item-list">
					<div class="row">
						<form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
							<div class="col-md-4 col-sm-12 col-xs-12">
				                <div class="sec-title">
									<h3 class="left">Create an account</h3><br/>
									<div>
										<h5>Username</h5>
										<input type="text" class="form-control" name="username">
										@if ($errors->has('username'))
		                                    <span class="help-block">
		                                        <p class="text-danger">{{ $errors->first('username') }}</p>
		                                    </span>
		                                @endif
										<h6>Your username is what other traders will know you by, so try to pick something memorable.</h6>
									</div><br/>
									<div>
										<h5>Password</h5>
										<input type="password" class="form-control" name="password">
										 @if ($errors->has('password'))
		                                    <span class="help-block">
		                                          <p class="text-danger">{{ $errors->first('password') }}</p>
		                                    </span>
		                                @endif
									</div><br/>
									<div>
										<h5>Confirm password</h5>
										<input type="password" class="form-control" name="password_confirmation">
										<h6>Remember, there is absolutely no way for localethereum staff to recover a lost password due to the nature of client-side encryption.</h6>
									</div><br/>
									<div>
										<h5>E-mail address</h5>
										<input type="email" class="form-control" name="email">
										<h6>Your e-mail address will be used for two-factor authentication.</h6>
										   @if ($errors->has('email'))
			                                    <span class="help-block">
			                                         <p class="text-danger">{{ $errors->first('email') }}</p>
			                                    </span>
			                                @endif
									</div><br/>
									<div>
										<!-- <p>Your web browser is going to generate a private key offline, and then encrypt it using AES256-CBC to a PBKDF2-stretched version of your password.</p> -->
									</div><br/>
									<div>
										<!-- <p>This means that our staff cannot access your wallet or read your messages.</p> -->
									</div><br/>
									<br/>

									{!! NoCaptcha::display() !!}
									@if ($errors->has('g-recaptcha-response'))
									    <span class="help-block">
									          <p class="text-danger">{{ $errors->first('g-recaptcha-response') }}</p>
									    </span>
									@endif

								</div>
				                
				                <!-- <ul class="about-links text-left">
									<li><button type="submit" class="thm-btn style-two">GENERATE KEYS & SIGN UP</button></li>
									
								</ul> -->
								<button type="submit" class="thm-btn style-two">GENERATE KEYS & SIGN UP</button>
				              </div>
							<div class="col-md-5 col-sm-10 col-xs-12">
						</form>
							<!-- <div class="item">
								<figure class="image-box">
									<img src="images/about/1.png" alt="" />
								</figure>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection