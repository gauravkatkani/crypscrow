@extends('layouts.frontend.masterone')
@section('content')
<section class="about">
			<div class="container">
				<div class="item-list">
					<div class="row">
						@if($errors->any())
						<h4>{{$errors->first('msg')}}</h4>
						@endif
						<form class="form-horizontal" method="POST" action="{{ route('logins') }}">
                        {{ csrf_field() }}
						<div class="col-md-4 col-sm-6 col-xs-12">
			                <div class="sec-title">
								<h3 class="left">Log in</h3><br/>
								 <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<h5>Username</h5>
									<input class="form-control" type="text" name="email" placeholder="">
									@if ($errors->has('email'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
                                	@endif
								</div><br/>
								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									<h5>Password</h5>
									<input class="form-control" type="password" name="password" placeholder="">
									@if ($errors->has('password'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('password') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>
			                
			                <ul class="about-links text-left">
								<li><button type="submit" class="thm-btn style-two">LOG IN</button></li>
								
							</ul>
							<div>
									<p>Don't have an account yet? 
										<b><a href="signup.html"> Create a new account.</a></b></p>
								</div><br/>

			              </div>
			          </form>
						<div class="col-md-5 col-sm-10 col-xs-12">
							<!-- <div class="item">
								<figure class="image-box">
									<img src="images/about/1.png" alt="" />
								</figure>
							</div> -->
						</div>
					</div>
				</div>
			</div>
		</section>
@endsection		