@extends('layouts.frontend.masterone')

@section('content')
<section class="about">
            <div class="container">
                <div class="item-list">
                    <div class="row">
                        
                        <div class="col-md-12 profileDiv col-sm-12 col-xs-12">
                            <div id="formerrors" class="text-danger"></div>
                            <div class="sec-title">
                                <h3 class="left">Edit account</h3>
                                <br/>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <h5>E-mail address</h5>
                                        <input class="form-control  input" type="email" name="email" value="{{Auth::user()->email}}">
                                        <h6>Your email address is never shown to anyone. To change it, we'll send you a confirmation link.</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <h5>Blurb</h5>
                                        <input class="form-control input" type="text" name="blurb" value="{{Auth::user()->blurb}}">
                                        <h6>Introduce yourself. This will be displayed on your public profile.</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <h5>Phone number</h5>
                                        <input class="form-control input" type="text" name="phone" value="{{Auth::user()->phone}}">
                                        <h6>Your phone number will never be shown to anyone. To change it, we'll send you a confirmation code.</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                                    <div class="col-md-4 col-sm-12 col-xs-12">
                                        <h5>Two factor log in method</h5>
                                        <select class="form-control authType input" name="authType">
                                            @foreach(Helper::getAuthTypes() as $authType)
                                                @php
                                                    $selected = "";
                                                    if(Auth::user()->authmethod==$authType->id)
                                                      $selected = "selected";

                                                @endphp
                                                <option value="{{$authType->id}}" {{$selected}}>{{$authType->name}}</option>
                                            @endforeach
                                        </select>
                                        <h6>Two-factor authentication is mandatory on localethereum. You can either use a link sent to your email, or an OTP app (e.g. Google Authenticator).</h6>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12"></div>
                                </div>
                                
                            </div>
                            
                            <ul class="about-links text-left">
                                <li><button  class="thm-btn saveProfile style-two">UPDATE ACCOUNT</button></li>
                                
                            </ul>

                          </div>
                        <div class="col-md-5 col-sm-10 col-xs-12">
                            <!-- <div class="item">
                                <figure class="image-box">
                                    <img src="images/about/1.png" alt="" />
                                </figure>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <img src="{{ $secreturl }}" alt="">
        <input name="authotp" class="input">
        <input name="action" value="2fa" class="input">
        <input name="secretKey" value="{{$secret}}" class="input">
        <button  class="thm-btn enable2fac style-two">Enable</button>
      </div>
         @if (Auth::user()->google2fa_secret)
                    <a href="{{ url('2fa/disable') }}" class="btn btn-warning">Disable 2FA</a>
                    @else
                    <a href="{{ url('2fa/enable') }}" class="btn btn-primary">Enable 2FA</a>
                    @endif
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
        @push('scripts')

            <script type="text/javascript">
            
               var  url     =    "{{url('profile')}}";

            </script>
            <script src="{{ asset('js/profile/profile.js') }}"></script>

        @endpush
@endsection
