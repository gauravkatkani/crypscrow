@extends('super-admin.layouts.master')

@section('content')
   <div class="row">
    <div class="col-xs-12">
          
       
            
                <h3 class="box-title">User List</h3>
               
           
       
                <table id="userlist_list_table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>User Name</th>
                            <th>Email</th>
                            
                            <th>Action</th>
                            <th>Privilege</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                
                </table>
     
    </div>
    <!-- /.col -->
</div>
@push('scripts')

  <script type="text/javascript">
    
    var url         =    "{{route('super-admin.user.ajax')}}";
   
  </script>
   

    <script src="{{ asset('super-admin/users/users.js') }}"></script>
@endpush
@endsection